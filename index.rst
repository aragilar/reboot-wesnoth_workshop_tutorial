Reboot Game Modding Workshop Tutorial and Guide
====================================================

In this workshop, we will be creating addons for
`Battle for Wesnoth`_.
We will be focusing on creating new units and factions,
although you can create maps and scenarios if you want to
(I suspect you won’t have time to create a campaign).

`Battle for Wesnoth`_ has excellent resources for modding
(see http://wiki.wesnoth.org/Create),
and this document will refer to them as needed.

.. _`Battle for Wesnoth`: http://www.wesnoth.org/

Contents:

.. toctree::
    :maxdepth: 2

    Setup
    Addon
    Unit
    Extensions

.. highlight:: none
   :linenothreshold: 5

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

