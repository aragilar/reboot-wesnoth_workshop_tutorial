Where You Go From Here
======================

We’ve only scratched the surface of what you can create.
Here’s some ideas for what you can do:

* Create more units: you probably won’t have time to make a whole faction,
  but you could create a set of units which advance into each other.
* Animate your unit: units have attack, defend and idle animations,
  you could create some for your unit.
* Add more unit properties: you could modify terrain movement cost,
  defence percentages or type resistances.
* Create a profile image: units have a large profile image,
  you could create one for your unit.

Presenting your work
--------------------

At the end of the camp,
we’re going to have an expo of what everyone has created over the camp.
We’ll create a new faction make of everyone’s units,
and let people play with them.
