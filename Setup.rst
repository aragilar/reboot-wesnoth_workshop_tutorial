Setting up your computer
========================

The tools of the trade
----------------------

In order to create new units,
we need some tools.

First, we need `Battle for Wesnoth`_.
This should already be on the computer.
Where exactly it is will depend on the computer
(look for it in the menus).

Next, we need an image editor for PNG_ files.
You will either have GIMP_ or Photoshop_ installed,
and you will use this to create
the images of the units we will be making.

Finally, we need a `text editor`_.
On Windows, `Notepad++`_ is what you will use.
On OS X, TextEdit_ is what you want.
On the `Raspberry Pis`_
(which run Linux_, specifically Debian_),
Text Editor
(also known as Gedit_)
will be your tool.

Where we put our stuff
----------------------

`Battle for Wesnoth`_ has special directories for addons.
The location of this directory depends on which OS you use.
http://wiki.wesnoth.org/EditingWesnoth
explains where these directories are,
however you can always find them by going to
Preferences → General → Paths from the main menu.

The specific directory we are looking for is the user data directory,
and it is in the this directory we need to create the directory ``add-ons``.


.. _`Battle for Wesnoth`: http://www.wesnoth.org/
.. _PNG: https://en.wikipedia.org/wiki/Portable_Network_Graphics
.. _GIMP: https://en.wikipedia.org/wiki/GIMP
.. _Photoshop: https://en.wikipedia.org/wiki/Adobe_Photoshop
.. _`text editor`: https://en.wikipedia.org/wiki/Text_editor
.. _`Notepad++`: https://en.wikipedia.org/wiki/Notepad%2B%2B
.. _TextEdit: https://en.wikipedia.org/wiki/TextEdit
.. _`Raspberry Pis`: https://en.wikipedia.org/wiki/Raspberry_Pi
.. _Linux: https://en.wikipedia.org/wiki/Linux
.. _Debian: https://en.wikipedia.org/wiki/Debian
.. _Gedit: https://en.wikipedia.org/wiki/Gedit
