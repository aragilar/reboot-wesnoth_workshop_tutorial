Creating and Using Units
========================

Now we get to the fun part,
creating new units.
Because there is so much we can do,
we're going to focus on creating one unit,
and only look at modifying a few bits.

There is a guide for creating units at
http://wiki.wesnoth.org/BuildingUnits,
but the basics will be given here.
See http://wiki.wesnoth.org/UnitTypeWML and
http://units.wesnoth.org/1.12/mainline/en_US/mainline.html
for what is possible.

Creating a Unit Image
---------------------

http://wiki.wesnoth.org/Creating_Unit_Art
provides some rules for creating your unit image,
but here’s a short summary:

    * The image must be 72x72 pixels

    * It must be a PNG

Creating images from scratch is hard,
so Battle for Wesnoth provides `frakenpacks`_,
collections of unit parts which can be “glued together”
to create a new unit.
I’ll give you a copy of the images so you don’t need to download them.

Save your image in the ``images/units`` directory,
giving it a useful name.

.. _frakenpacks: http://wiki.wesnoth.org/FrankenPacks

Setting unit attributes
-----------------------

To set the properties of a unit,
we need to create a file inside the ``units`` directory of your add-on.

Here’s an example of what you need in that file:

.. code-block:: none
    :linenos:

    [unit_type]
        id=Vampire
        name= _ "Vampire"
        race=undead
        image="units/vampire.png"
        ellipse="misc/ellipse"
        {MAGENTA_IS_THE_TEAM_COLOR}
        hitpoints=34
        movement_type=undeadfoot
        movement=7
        experience=35
        level=1
        alignment=chaotic
        advances_to=null
        cost=20
        usage=fighter
        description= _ "Vampires are creatures of the night, looking to feed on unsuspecting victims."+{SPECIAL_NOTES}+{SPECIAL_NOTES_DRAIN}+{SPECIAL_NOTES_POISON}+{SPECIAL_NOTES_PLAGUE}+{SPECIAL_NOTES_BACKSTAB}
        [abilities]
            {ABILITY_SKIRMISHER}
        [/abilities]
        [attack]
            name=sword
            description=_"sword"
            icon=attacks/longsword.png
            type=blade
            range=melee
            damage=4
            number=5
            [specials]
                {WEAPON_SPECIAL_BACKSTAB}
            [/specials]
        [/attack]
        [attack]
            name=fangs
            description=_"fangs"
            icon=attacks/fangs.png
            type=pierce
            range=melee
            damage=10
            number=1
            [specials]
                {WEAPON_SPECIAL_DRAIN}
                {WEAPON_SPECIAL_POISON}
                {WEAPON_SPECIAL_PLAGUE}
            [/specials]
        [/attack]
    [/unit_type]

There's a number of things you will need to set to get your unit working:

    1. Set the path of the unit image (i.e. ``image``) to the image file you created.
    2. Set the ``id`` and ``name`` to something unique.

You also would want to modify the ``description``, ``cost`` etc. to make your
unit your own.

Adding your unit to a faction/era
---------------------------------

To actually play with your unit, you need to add it to a faction in a era.
The easiest thing to do is create a new era from the default era.
To do this, create an ``era.cfg`` file in your add-on directory,
with the following contents (replacing ``Vampire`` with your unit name):

.. code-block:: none

    [era]
        id=core_and_vampires
        name= _ "Core and Vampires"
        {RANDOM_SIDE}
        {multiplayer/factions/loyalists-default.cfg}
        {multiplayer/factions/rebels-default.cfg}
        {multiplayer/factions/northerners-default.cfg}
        {multiplayer/factions/knalgans-default.cfg}
        {multiplayer/factions/drakes-default.cfg}

        [multiplayer_side]
            id=Undead
            name= _"Undead"
            image="units/undead-necromancers/dark-sorcerer.png"
            {MAGENTA_IS_THE_TEAM_COLOR}
            type=Dark Sorcerer
            leader=Dark Sorcerer,Revenant,Deathblade,Bone Shooter,Necrophage
            random_leader=Dark Sorcerer,Revenant,Deathblade,Bone Shooter
            recruit=Skeleton,Skeleton Archer,Walking Corpse,Ghost,Vampire Bat,Dark Adept,Ghoul,Vampire
            terrain_liked=Ss, Hh, Ha, Ww
            [ai]
                recruitment_pattern=fighter,fighter,archer,fighter,scout,archer
            [/ai]
        [/multiplayer_side]
    [/era]

This has added your unit to the Undead faction.
You could add it to a different faction, by changing what appears inside the
``multiplayer_side`` section.

Further information on factions and eras appears at
http://wiki.wesnoth.org/BuildingFactions and
http://wiki.wesnoth.org/EraWML.
