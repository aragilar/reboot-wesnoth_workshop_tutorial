Creating an Add-On
==================

This is a short introduction to creating an add-on,
see `Addon Structure`_ for more information
about the correct structure of an add-on.

Add-on Layout
-------------

To create an add-on,
you need to create a directory inside the ``add-ons`` directory
with a name like ``My_Amazing_Era``.
The important part is that there should be no spaces
(Underscores are used instead of spaces).
This will hold all the things that you will create.

Inside this directory, you need to create a text file,
using the text editor, named ``_main.cfg``.
This will hold the settings for your add-on.

In addition,
you will need to create two more directories
inside your add-on directory,
``units`` and ``images/units``
(there are other directories that go inside your add-on directory,
but this is all we will need to create some units and play with them).

The ``_main.cfg`` file
----------------------

The contents of the ``_main.cfg`` file set how your add-on is loaded.
For our purposes, we want something like this:

.. code-block:: none
    :linenos:

    #textdomain wesnoth-My_Amazing_Era

    #ifdef MULTIPLAYER
    [binary_path]
        path=data/add-ons/My_Amazing_Era
    [/binary_path]

    {~add-ons/My_Amazing_Era/era.cfg}

    [+units]
        {~add-ons/My_Amazing_Era/units}
    [/units]
    #endif

You should be able to work out
what each of these things do using `Addon Structure`_,
and if you create maps/scenarios,
how to add them to your ``_main.cfg``.

.. _`Addon Structure`: http://wiki.wesnoth.org/AddonStructure
